package main

import (
	"fmt"
	"os"
	"os/exec"
)

func main() {
	args := os.Args[1:]

	cloneURL := args[0]
	projectDir := args[1]
	fmt.Printf("Setting up %s Project dir: %s\n", cloneURL, projectDir)
	err := clone(cloneURL, projectDir)

	if err != nil {
		panic(err)
	}

	err = os.Chdir(projectDir)
	if err != nil {
		panic(err)
	}

	err = runComposer(projectDir)

	if err != nil {
		panic(err)
	}

	err = setEnv()

	if err != nil {
		panic(err)
	}

	err = generateKey()

	if err != nil {
		panic(err)
	}

	fmt.Println("All set!")

}

func run(cmd *exec.Cmd) error {
	cmd.Stderr = os.Stderr

	_, err := cmd.Output()

	if err != nil {
		return err
	}
	return nil
}

func generateKey() error {
	keyCmd := exec.Command("php", "artisan", "key:generate")

	return run(keyCmd)
}

func setEnv() error {
	cpCmd := exec.Command("cp", ".env.example", ".env")

	return run(cpCmd)
}

func runComposer(projectDir string) error {
	composerCmd := exec.Command("composer", "install")
	return run(composerCmd)
}

func clone(cloneURL, projectDir string) error {
	cloneCmd := exec.Command("git", "clone", cloneURL, projectDir)
	return run(cloneCmd)
}
